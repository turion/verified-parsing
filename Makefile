.PHONY:

AGDA=agda

AGDA_MAIN=\
	src/Main.agda \

AGDA_SRC=\
	src/SortedVec.agda \

all:
	$(MAKE) agda
	$(MAKE) haskell

agda: $(AGDA_SRC) $(AGDA_MAIN)

$(AGDA_SRC): phony
	$(AGDA) -c --ghc-dont-call-ghc --no-main $@

$(AGDA_MAIN): phony
	$(AGDA) -c --ghc-dont-call-ghc $@

haskell:
	cabal build

phony:
