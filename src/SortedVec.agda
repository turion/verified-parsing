module SortedVec where

open import Ord using (Ord; _≤_; _≤?_; ≤-isDecTotalOrder; ≤-trans; _≤!_; ≤-refl)
import Data.Nat as ℕ
open ℕ using (ℕ)
import Data.Nat.Properties as ℕ
open import Relation.Nullary
open import Relation.Nullary.Sum using (_¬-⊎_)
open import Data.Sum
open import Relation.Binary.PropositionalEquality hiding (trans)
open import Data.Unit using (⊤; tt)
open import Data.Product using (_×_; _,_)
open import Data.Fin using (Fin)
open import Relation.Binary using (Decidable)
import Data.Fin as Fin
open Data.Fin using (Fin′)
open import Data.Maybe using (Maybe; just; nothing)
open import Data.Vec using (Vec; _∷_; [])
open import Data.Product using (proj₁; proj₂)

infixr 5 _∷_
data SortedVec (A : Set) ⦃ _ : Ord A ⦄ : ℕ → {A} → Set where
  []  : {x : A} → SortedVec A 0 {x}
  _∷_ : {n : ℕ}
      → (x : A)
      → (xs : SortedVec A n {x}) -- vec allowed to prepend x
      → {y : A}
      → {x≤y : x ≤ y}
      → SortedVec A (ℕ.suc n) {y} -- vec with largest element at most y, larger that

head : {A : Set} ⦃ _ : Ord A ⦄ {n : ℕ} {x : A} → SortedVec A (ℕ.suc n) {x} → A
head (x ∷ _) = x

infixr 26 _!!_
_!!_ : {A : Set}⦃ _ : Ord A ⦄{n : ℕ}{x : A} → SortedVec A n {x} → (m : Fin n) → A
(x ∷ _ ) !! Fin.zero = x
(x ∷ xs) !! Fin.suc v = xs !! v

vᵢ≤y : {A : Set}⦃ _ : Ord A ⦄{n : ℕ}{y : A}
     → (v : SortedVec A n {y})
     → (i : Fin n)
     → v !! i ≤ y
vᵢ≤y ((v ∷ vs) {x≤y = v≤y}) Fin.zero = v≤y
vᵢ≤y ((v ∷ vs) {x≤y = v≤y}) (Fin.suc i) = ≤-trans (vᵢ≤y vs i) v≤y

sorted : {A : Set}⦃ _ : Ord A ⦄{n : ℕ}{x : A}
       → (v : SortedVec A n {x})
       → (i : Fin n)  -- i < n
       → (j : Fin′ i) -- j < i
       → (v !! i) ≤ (v !! Fin.inject j)
sorted ((x ∷ v) {y} {x≤y}) (Fin.suc i) Fin.zero = ≤-trans (vᵢ≤y v i) ≤-refl
sorted (x ∷ v) (Fin.suc i@(Fin.suc _)) (Fin.suc j) = sorted v i j

insert : {A : Set}⦃ _ : Ord A ⦄{n : ℕ} {a : A}
       → (x : A)
       → {x ≤ a}
       → SortedVec A n {a}
       → SortedVec A (ℕ.suc n) {a}
insert {a = a} x {p} [] = (x ∷ []) {x≤y = p}
insert {a = a} x {p} ((y ∷ ys) {_} {y≤a}) with y ≤! x
... | inj₁ y≤x = (x ∷ ((y ∷ ys) {x} {y≤x})) {a} {p}
... | inj₂ x≤y = (y ∷ insert x {x≤y} ys) {a} {y≤a}

allV : {A : Set}{n : ℕ} → (A → Set) → Vec A n → Set
allV p [] = ⊤
allV p (x ∷ xs) = p x × allV p xs

sort : {A : Set}⦃ _ : Ord A ⦄{n : ℕ}{y : A}
     → (v : Vec A n)
     → {allV (_≤ y) v}
     → SortedVec A n {y}
sort [] = []
sort (x ∷ xs) {all≤y} = insert x {proj₁ all≤y} (sort xs {proj₂ all≤y})

allV? : {A : Set}{n : ℕ} → {P : A → Set} → ((x : A) → Dec (P x)) → (v : Vec A n) → Dec (allV P v)
allV? p [] = yes tt
allV? p (x ∷ xs) with p x    | allV? p xs
...                 | yes px  | yes all-pxs  = yes (px , all-pxs)
...                 | no ¬px  | _            = no λ all-px∷xs → ¬px (proj₁ all-px∷xs)
...                 | yes _   | no ¬all-pxs  = no λ all-px∷xs → ¬all-pxs (proj₂ all-px∷xs)

isSorted? : {A : Set}⦃ _ : Ord A ⦄{n : ℕ}{z : A}
          → (v : Vec A n)
          → {all≤z : allV (_≤ z) v}
          → Maybe (SortedVec A n {z})
isSorted? [] = just []
isSorted? (x ∷ xs) {all≤z = all≤z} with  allV? (_≤? x) xs
...                       | no ¬all-xs≤x = nothing
...                       | yes all-xs≤x with isSorted? xs {all≤z = all-xs≤x}
...                                         | just sorted-xs = just ((x ∷ sorted-xs) {x≤y = proj₁ all≤z})
...                                         | nothing = nothing
