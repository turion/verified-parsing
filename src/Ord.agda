module Ord where

import Level
open Level using (Level)
open import Relation.Nullary using (Dec)
open import Relation.Binary
open import Eq

record Ord {ℓ : Level} (A : Set ℓ) : Set (Level.suc ℓ) where
  field
    ⦃ eqA ⦄ : Eq A
    _≤_ : Rel A ℓ
    ≤-isDecTotalOrder : IsDecTotalOrder _≡_ _≤_

  ≤-trans = IsDecTotalOrder.trans ≤-isDecTotalOrder
  ≤-refl = IsDecTotalOrder.refl ≤-isDecTotalOrder
  _≤?_ = IsDecTotalOrder._≤?_ ≤-isDecTotalOrder
  _≤!_ = IsDecTotalOrder.total ≤-isDecTotalOrder

open Ord ⦃...⦄ public

import Data.Nat as ℕ
open import Data.Nat using (ℕ)
import Data.Nat.Properties as ℕ

instance
  NatOrd : Ord ℕ
  eqA ⦃ NatOrd ⦄ = NatEq
  _≤_ ⦃ NatOrd ⦄ = ℕ._≤_
  ≤-isDecTotalOrder ⦃ NatOrd ⦄ = ℕ.≤-isDecTotalOrder

