module Main where

open import Agda.Builtin.IO as Builtin
open import Data.Unit
open import IO

main : Builtin.IO ⊤
main = run (putStrLn "Hallo")
