module Eq where

import Level
open Level using (Level)
open import Relation.Nullary using (Dec)
open import Relation.Binary

record Eq {ℓ : Level} (A : Set ℓ) : Set (Level.suc ℓ) where
  field
    _≡_ : Rel A ℓ
    ≡-isDecEquivalence : IsDecEquivalence _≡_

  _≟_ = IsDecEquivalence._≟_ ≡-isDecEquivalence

open Eq ⦃...⦄ public

open import Data.Nat using (ℕ)
import Data.Nat.Properties as ℕ
import Relation.Binary.PropositionalEquality renaming (_≡_ to _==_)
open Relation.Binary.PropositionalEquality using (_==_)

instance
  NatEq : Eq ℕ
  _≡_ ⦃ NatEq ⦄ = _==_
  ≡-isDecEquivalence ⦃ NatEq ⦄ = ℕ.≡-isDecEquivalence
